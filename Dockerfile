ARG rb_version
ARG os_version
FROM ruby:${rb_version}-slim-${os_version}

ADD spell /usr/local/bin/spell
ADD . /
RUN apt-get update && \
    apt-get install python3-restructuredtext-lint shunit2 spell -y && \
    rm -rf /var/lib/apt/lists/* && \
    gem install mdl && \
    chmod +x /usr/local/bin/spell && \
    chmod +x /tests/*.sh && \
    chmod +x /runtests.sh && \
    ./runtests.sh;

ENTRYPOINT /bin/bash
