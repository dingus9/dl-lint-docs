#!/bin/bash

echo "[INFO] Testing README.md for spelling errors using spell and .spellrc from repo."
spell -d ../.spellrc ../README.md
